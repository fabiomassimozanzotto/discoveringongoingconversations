from keras.models import Sequential
from keras.layers import Dense, Activation
import keras.optimizers as opt
import numpy as np
import corpusReader as cr
import evaluator as e


import sys, getopt

def getParameters(argv) :
    train = ""
    test = ""
    blocks = ""
    dim = ""
    try:
        opts, args = getopt.getopt(argv, "h", ["train=", "test=","dim=", "blocks="])
    except getopt.GetoptError:
        print('baseExperiment.py --train=<train> --test=<test> --blocks=<blocks> --dim=<dt_dim>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('baseExperiment.py --train=<train> --test=<test> --blocks=<blocks> --dim=<dt_dim>')
            sys.exit()
        elif opt== "--train":
            train = arg
        elif opt == "--test":
            test = arg
        elif opt == "--dim":
            dim = int(arg)
        elif opt == "--blocks":
            blocks = arg
    return train, test, blocks, dim


def main(argv):
    train, test, blocks, distrTreeDim = getParameters(argv)
    max_num_of_words = 40000
    inputs = []

    X_train,X_train_specific_features,Y_train,max_value,_,_ = cr.read_corpus(train,blocks, 3,n_of_words=max_num_of_words)
    X_test,X_test_specific_features,Y_test,max1,Y_test_table,_ = cr.read_corpus(test, blocks, 3,max_lenght_of=max_value,n_of_words=max_num_of_words)

    standard_pair_feature_model = Sequential()
    standard_pair_feature_model.add(Dense(input_dim=9, output_dim=9))
    standard_pair_feature_model.add(Activation("relu"))
    standard_pair_feature_model.add(Dense(input_dim=9, output_dim=2))
    standard_pair_feature_model.add(Activation("relu"))
    standard_pair_feature_model.add(Activation("softmax"))

    final_model = standard_pair_feature_model
    print("compiling...", end="")

    optim = opt.RMSprop(lr=0.00001, rho=0.9, epsilon=1e-08, decay=0.0)

    final_model.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['accuracy'])
    print("done")
    for i in range(0,500):
        X_train_specific_features = np.asarray(X_train_specific_features)
        final_model.fit(X_train_specific_features, Y_train, nb_epoch=1, batch_size=16, class_weight=[0.999, 0.001])

        print("Evaluation ", i)
        X_test_specific_features = np.asarray(X_test_specific_features)
        system_predictions = final_model.predict(X_test_specific_features, batch_size=300, verbose=1)

        print("Generating tables ", i)
        oracle_table = e.generate_table(Y_test_table, Y_test)
        system_table = e.generate_table(Y_test_table, system_predictions)

        print("Computing performance ", i)
        recall_at_k_res = e.recall_at_k(oracle_table, system_table, k_s=[1, 2, 5, 10, 100, 1000])

        print("Result (Test Set) : (", i, ")", recall_at_k_res)


if __name__ == "__main__":
   main(sys.argv[1:])


