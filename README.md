# README #

### What is this repository for? ###

* This is a python package to use the Discovery Ongoing Conversation corpus in Python and in KERAS
* Version 1.0


### How do I get set up? ###

* Dependencies: KERAS, DOC corpus (http://art.uniroma2.it/zanzotto/research/doc/)
* Main Module: baseExperiment.py


